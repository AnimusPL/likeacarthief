extends Area2D

var velocity

func _ready():
	$BulletSprite.position = Vector2(0, -50)
	$BulletCollisionShape.position = Vector2(0, -50)

func _physics_process(delta):
	global_position += velocity * delta

func _on_Bullet_body_entered(body):
	if body.is_in_group("cars") and body != self:
		get_node(str(body.get_path()) + '/CarImpact').play()
		get_node(str(body.get_path()) + '/CarImpact').pitch_scale = rand_range(0.9, 1.1)
		body.hp -= 10
		queue_free()
