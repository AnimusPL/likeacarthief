extends RigidBody2D

export var emitting : bool = true

func _ready():
	if emitting == true:
		if !$Ringing.playing: $Ringing.play()
		$SpeakerEmitter.emitting = true

func _physics_process(_delta):
	pass

func _on_Area2D_body_entered(body):
#	get_tree().get_root().get_node("player")
#	if body = get_tree().get_root().get_node("Player"):
#	if body.is_in_group("Player"):
	if body.get_name() == "Player":
		$Ringing.stop()
		$SpeakerEmitter.emitting = false

func _on_DestroyPhoneArea_body_entered(body):
	if body.is_in_group("cars"):
		$Ringing.stop()
		$SpeakerEmitter.emitting = false
		$PhysicCollision.set_deferred("disabled", true)
		$AnimatedSprite.animation = "broken"
		$AnimatedSprite.rotation_degrees = rand_range(1,360)
