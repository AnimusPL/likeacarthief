extends KinematicBody2D

export (float) var max_speed = 1500
export var base_acceleration : float = 3.5
export var hp : int = 100

export var exploded = false

var acceleration : float = 4
var braking_power = 2.5
var rotation_dir: float = 0.0
var velocity = Vector2()
var actual_speed: float = 0
var backward_reverse = false
var engine_is_on = false
var gearbox_shift : int
var inertness: int
var car_input_active = false
var hitting_another_car = false
var colliding_car = KinematicBody2D.new()

func _ready():

	# EACH CAR HAS INDIVIDUAL CARSCREECHING SCENE LOADED ON MAIN
	var CarScreeching = load("res://CarScreeching.tscn").instance()
	get_node('/root/Main').call_deferred('add_child', CarScreeching)

	# UP AND DOWN KEYS INPUTS
func accelerating_and_slowing_down():
	if Input.is_action_pressed('up') and actual_speed < max_speed:
		actual_speed += acceleration
		$ExhaustParticle.process_material.scale = 0.05
		if actual_speed < 0:
			slowing_down(7)
	elif Input.is_action_pressed('down') and actual_speed > -max_speed/4:
		actual_speed -= acceleration
		$ExhaustParticle.process_material.scale = 0.05
		if actual_speed > 0:
			slowing_down(7)
	else:
		slowing_down(3)
		$ExhaustParticle.process_material.scale = 0.03
	reducing_inertness(15)
		
	# SPACE KEY INPUT
func braking():
	if Input.is_action_pressed('space'):
		slowing_down(7)

	# RIGHT AND LEFT KEYS INPUTS
func left_and_right_steering():
		if Input.is_action_pressed('right'):
			rotation_dir = 1.7 - (actual_speed/500)/2
			if actual_speed > 500:
				if inertness > -1000: 
					inertness -= 30
				braking_marks()
			
		elif Input.is_action_pressed('left'):
			rotation_dir = -1.7 + (actual_speed/500)/2
			if actual_speed > 500:
				if inertness < 1000: 
					inertness += 30
				braking_marks()
		else:
			rotation_dir = 0.0

	# FUNCTION WHICH TURNS DOWN THE SPEED OF CAR AND REDUCING INERTNESS
func slowing_down(num):
	if actual_speed > 0:
		if actual_speed < num:
			actual_speed = 0
		else:
			actual_speed -= num ###############
	elif actual_speed < 0:
		if actual_speed > -num:
			actual_speed = 0
		else:
			actual_speed += num ###############
	if num >= 7 and abs(actual_speed) > 0:
		braking_marks()
		if $BrakeScreech.playing == false: 
			$BrakeScreech.play()
	reducing_inertness(15)

	# CREATING MARKS WHILE BRAKING IS ACTIVE
func braking_marks():
	get_tree().set_group("BrakingMarks", "emitting", true)

	# CREATING BRAKING SOUNDS WHILE INERTNESS IS OTHER THAN 0
func brake_screeching():
	if inertness > 0:
		braking_marks()
		if $BrakeScreech.playing == false: 
			$BrakeScreech.play()
	elif inertness < 0:
		braking_marks()
		if $BrakeScreech.playing == false: 
			$BrakeScreech.play()
	else:
		if inertness == 0: $BrakeScreech.stop()

	# REDUCING INERTNESS ON IDLE
func reducing_inertness(num):
	if inertness > 0:
		inertness -= num
	elif inertness < 0:
		inertness += num

	# GEARBOX SHIFTING CORELATED WITH ENGINE SOUND'S PITCH
func set_gearbox_shifting(shift1_max_speed_shift2_min_speed, shift1_sound_divider, shift1_acceleration_reducer, 
						shift2_max_speed_shift3_minspeed, shift2_sound_divider, shift2_acceleration_reducer, 
						shift3_max_speed_shift4_minspeed, shift3_sound_divider, shift3_acceleration_reducer, 
						shift4_max_speed_shift5_minspeed, shift4_sound_divider, shift4_acceleration_reducer, 
						shift5_sound_divider, shift5_acceleration_reducer):
	#SHIFT 1
	if abs(actual_speed) > 0 and actual_speed <= shift1_max_speed_shift2_min_speed: 
		$EngineSound.pitch_scale = 1 + abs(actual_speed / (shift1_sound_divider * 10))
		acceleration = base_acceleration - shift1_acceleration_reducer
		gearbox_shift = 1
	#SHIFT 2
	elif abs(actual_speed) > shift1_max_speed_shift2_min_speed and actual_speed <= shift2_max_speed_shift3_minspeed: 
		$EngineSound.pitch_scale = 1 + abs(actual_speed / (shift2_sound_divider * 10))
		acceleration = base_acceleration - shift2_acceleration_reducer
		gearbox_shift = 2
	#SHIFT 3
	elif abs(actual_speed) > shift2_max_speed_shift3_minspeed and actual_speed <= shift3_max_speed_shift4_minspeed: 
		$EngineSound.pitch_scale = 1 + abs(actual_speed / (shift3_sound_divider * 10))
		acceleration = base_acceleration - shift3_acceleration_reducer
		gearbox_shift = 3
	#SHIFT 4
	elif abs(actual_speed) > shift3_max_speed_shift4_minspeed and actual_speed <= shift4_max_speed_shift5_minspeed: 
		$EngineSound.pitch_scale = 1 + abs(actual_speed / (shift4_sound_divider * 10))
		acceleration = base_acceleration - shift4_acceleration_reducer
		gearbox_shift = 4
	#SHIFT 5
	elif abs(actual_speed) > shift4_max_speed_shift5_minspeed: 
		$EngineSound.pitch_scale = 1 + abs(actual_speed / (shift5_sound_divider * 10))
		acceleration = base_acceleration - shift5_acceleration_reducer
		gearbox_shift = 5
		
	else: $EngineSound.pitch_scale = 1
	
	# BEHAVIOR WHILE ENTER IS PRESSED IN CAR
func leaving_a_car():
	if Input.is_action_just_pressed('enter'):
		get_node('/root/Main/Player/CollisionShape2D').disabled = false
		get_node('/root/Main/Player').player_in_a_car = false
		get_node('/root/Main/Player').show()
		get_node('/root/Main/Player').position = position
		get_node('/root/Main/Player').rotation_degrees = rotation_degrees
		$HUD/Speedometer.text = ''
		$HUD/Shiftometer.text = ''
		car_input_active = false

#		$CollisionWithEnv/CollisionWithEnvShape.disabled = true
	
	# BEHAVIOR WHILE Q IS PRESSED IN CAR
func horn():
	if Input.is_action_pressed('q'):
		if $CarHorn.playing == false:
			$CarHorn.play()
	else: $CarHorn.stop()

func car_exploding_if_hp_below_0():
	if hp <= 0:
		$Bars/HealthBar.visible = false
		$AnimatedSprite.animation = "exploded"
		engine_is_on = false
		if exploded == false:
			$CarExplosionAnimation.play()
			$ExplosionSound.play()
			exploded = true
			$ExhaustParticle.emitting = false
	

	# MAIN LOOP OF CAR'S PHYSICS
func _physics_process(delta):
	# HP SETTINGS
	$Bars/HealthBar.value = hp
	$Bars.rotation = -rotation
	car_exploding_if_hp_below_0()

	if engine_is_on == true:
		$EngineSound.play()
	if car_input_active == true:
		get_input()
	if car_input_active == false:
		slowing_down(3)
	
	# SETTING A GEARBOX
	set_gearbox_shifting(300, 18, 1, 
						600, 35, 0, 
						900, 55, 1.2, 
						1000, 75, 2.8, 
						90, 3)
	brake_screeching()

	rotation += (rotation_dir * actual_speed * delta)/200
	velocity = Vector2(inertness, -actual_speed).rotated(rotation)
	velocity = move_and_slide(velocity)
	
	# THIS FUNCTION IS ACTIVE WHEN PLAYER IS IN A CAR
func get_input():
	$CollisionWithEnv/CollisionWithEnvShape.disabled = false
	$HUD/Speedometer.text = str(int(actual_speed/10)) + " km/h"
	$HUD/Shiftometer.text = str(gearbox_shift)
	
	#SETTING A CAMERA
	get_node('/root/Main/Camera2D2').position = position
	if get_node('/root/Main/Camera2D2').zoom.x < 1:
		get_node('/root/Main/Camera2D2').zoom += Vector2(0.01, 0.01)
	
	if get_node('/root/Main/Camera2D2').zoom.x >= 1:
		get_node('/root/Main/Camera2D2').zoom = Vector2(1, 1) + Vector2(abs(actual_speed)/2000, abs(actual_speed)/2000)
	
	get_tree().set_group("BrakingMarks", "emitting", false)
	leaving_a_car()
	left_and_right_steering()
	horn()
	braking()
	
	# BEHAVIOR WHILE PRESSING E - TURNING AN ENGINE ON OR OFF
	if engine_is_on == true:
		$ExhaustParticle.emitting = true
		accelerating_and_slowing_down()
		if Input.is_action_just_pressed("e"):
			engine_is_on = false
			$AnimatedSprite.animation = "engine_off"

	elif engine_is_on == false:
		$ExhaustParticle.emitting = false
		$EngineSound.stop()
		slowing_down(3)
		if Input.is_action_just_pressed("e") and hp > 0:
			engine_is_on = true
			$EngineStart.play()
			$AnimatedSprite.animation = "engine_on"
			
	get_node('/root/Main/CarScreeching').position = (position + colliding_car.position) / 2
	
	if hitting_another_car == true:
		actual_speed -= 0.05 * actual_speed
		if actual_speed != 0:
			if $MetalScreech.playing == false:
				$MetalScreech.play()
			get_node('/root/Main/CarScreeching').emitting = true

func _on_CollisionWithEnv_body_entered(body):
	if body.is_in_group("cars") and body != self:
		colliding_car = body
		hitting_another_car = true
		if (actual_speed > 300 or actual_speed < -300):
			play_car_hit_sound_and_animation(colliding_car)
			rotation_degrees += rand_range(-30.0, 30.0)
			hp -= 25
			colliding_car.hp -= 25
		actual_speed = actual_speed / 4
#
#	elif body.is_in_group("Bullets"):
#		hp -= 10
#		print('dupa')

func play_car_hit_sound_and_animation(body):
	$CarHit.play()
	get_node('/root/Main/CarHitAnimation').position = (position + body.position) / 2
	get_node('/root/Main/CarHitAnimation').play()

func _on_CollisionWithEnv_body_exited(body):
	if body.is_in_group("cars"):
		hitting_another_car = false
		$MetalScreech.stop()
		get_node('/root/Main/CarScreeching').emitting = false

