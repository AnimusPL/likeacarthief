extends KinematicBody2D

export (int) var speed = 200
export (float) var rotation_speed = 4.0

var velocity = Vector2()
var rotation_dir = 0
var player_in_a_car = false
var player_in_car_range = false
var body_colliding_with_a_player = null

const BULLET = preload("res://Bullet.tscn")
var reloading = 0.0

var weapons = []
export var pistol_ammo : int = 0
export var mp5_ammo : int = 0
var active_weapon = null
export var hp : int = 100


func _ready():
	$AnimatedSprite.play()
	
func health_bar_corelated_with_hp():
	$GUI/HealthBar.value = hp

func get_input():
	get_node('/root/Main/Camera2D2').position = position
	if get_node('/root/Main/Camera2D2').zoom.x > 1:
		get_node('/root/Main/Camera2D2').zoom -= Vector2(0.01, 0.01)
	rotation_dir = 0
	velocity = Vector2()
	
	#SETTING A CAMERA
	if get_node('/root/Main/Camera2D2').zoom.x > 0.7:
		get_node('/root/Main/Camera2D2').zoom -= Vector2(0.001, 0.001)
	
	if Input.is_action_pressed('right'):
		rotation_dir += 1
	elif Input.is_action_pressed('left'):
		rotation_dir -= 1
	if Input.is_action_pressed('down'):
		velocity = Vector2(0, speed).rotated(rotation)
		$AnimatedSprite.animation = "walk"
		if !$RunningSound.playing: $RunningSound.play()
	elif Input.is_action_pressed('up'):
		velocity = Vector2(0, -speed).rotated(rotation)
		$AnimatedSprite.animation = "walk"
		if !$RunningSound.playing: $RunningSound.play()
	else:
		$AnimatedSprite.animation = "idle"
		$RunningSound.stop()
	if Input.is_action_just_pressed('enter'):
		if body_colliding_with_a_player != null and body_colliding_with_a_player.is_in_group("cars"):
			$CollisionShape2D.disabled = true
			$RunningSound.stop()
			hide()
			velocity = Vector2(0, 0)
			player_in_a_car = true
			body_colliding_with_a_player.car_input_active = true
			body_colliding_with_a_player = null
			
	if Input.is_action_pressed("ctrl") and weapons != []:
		if $AnimatedSprite.animation != "walk": $AnimatedSprite.animation = 'pistol'
		fire()

func weapon_change(delta):
	if weapons != []:
		if weapons[0] == 'pistol':
			$GUI/WeaponSprite.texture = load("res://sprites/weapons/pistolx.png")
			$GUI/WeaponSprite.scale = Vector2(0.6, 0.6)
			$GUI/WeaponAmmo.text = str(pistol_ammo)
			
			reloading -= delta

		elif weapons[0] == 'mp5':
			$GUI/WeaponSprite.texture = load("res://sprites/weapons/mp5.png")
			$GUI/WeaponSprite.scale = Vector2(0.9, 0.9)
			$GUI/WeaponAmmo.text = str(mp5_ammo)
		
			reloading -= delta
			
		if Input.is_action_just_pressed("z"):
			weapons.invert()

func fire():
	if weapons[0] == 'pistol' and pistol_ammo > 0:
		if reloading <= 0.0:
			var bullet = BULLET.instance()
			bullet.global_position = position
			bullet.rotation = rotation
			bullet.velocity = Vector2(0, -500).rotated(rotation)
			get_parent().add_child(bullet)
			reloading = 0.6
			$GunShot.play()
			pistol_ammo -= 1
	elif weapons[0] == 'mp5' and mp5_ammo > 0:
		if reloading <= 0.0:
			var bullet = BULLET.instance()
			bullet.global_position = position
			bullet.rotation = rotation
			bullet.velocity = Vector2(0, -500).rotated(rotation)
			get_parent().add_child(bullet)
			reloading = 0.1
			$GunShot.play()
			mp5_ammo -= 1
			
func _physics_process(delta):
	health_bar_corelated_with_hp()
	weapon_change(delta)
	print(weapons)
	
	if player_in_a_car == false:
		get_input()
		
	rotation += rotation_dir * rotation_speed * delta
	velocity = move_and_slide(velocity)
	
	#GUI


func _on_CollisionArea_body_entered(body):
	body_colliding_with_a_player = body

func _on_CollisionArea_body_exited(_body):
	body_colliding_with_a_player = null
