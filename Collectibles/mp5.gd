extends Area2D

var AmmoNum : int = 30

func _ready():
	rotation_degrees = rand_range(0, 360)
	
func _physics_process(_delta):
	pass

func _on_MP5_body_entered(body):
	if body.is_in_group("Players"):
		body.mp5_ammo += AmmoNum
		if not 'mp5' in body.weapons:
			body.weapons.push_front('mp5')
		self.queue_free()
