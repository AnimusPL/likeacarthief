extends Area2D

var AmmoNum : int = 12

func _ready():
	rotation_degrees = rand_range(0, 360)
	
func _physics_process(_delta):
	pass

func _on_Pistol_body_entered(body):
	if body.is_in_group("Players"):
		body.pistol_ammo += AmmoNum
		if not 'pistol' in body.weapons:
			body.weapons.push_front('pistol')
		self.queue_free()
